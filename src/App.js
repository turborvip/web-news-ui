import './App.css';
import { useStore } from '../src/store'
import Home from './pages/Home';
import HeaderCommon from './common/HeaderCommon'
import FooterCommon from './common/FooterCommon'
import Navbar from './common/Navbar/Navbar'

function App() {
  const [state] = useStore();



  return (
    <div className="App">
      {
        state.auth &&
        <>
          <HeaderCommon />
          <Navbar />
          <Home />
          <FooterCommon />
        </>
      }

    </div>
  );
}

export default App;

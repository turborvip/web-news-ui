import { SET_SLIDE_IMAGE, SET_USERNAME_LOGIN, SET_PASSWORD_LOGIN, SET_OTP_STATUS, SET_ID_USER, SET_AUTH_USER } from "./constants";

const initState = {
    username: '',
    password: '',
    slidesImage: [],
    otpStatus: '',
    otp: '',
    idUser: '',
    auth: 3,

}



function reducer(state, action) {
    switch (action.type) {
        case SET_USERNAME_LOGIN:
            return {
                ...state,
                username: action.payload
            }
        case SET_PASSWORD_LOGIN:
            return {
                ...state,
                password: action.payload
            }
        case SET_OTP_STATUS:
            return {
                ...state,
                otpStatus: action.payload
            }
        case SET_ID_USER:
            return {
                ...state,
                idUser: action.payload
            }
        case SET_SLIDE_IMAGE:
            return {
                slidesImage: [action.payload]
            }
        case SET_AUTH_USER:
            return {
                ...state,
                auth: action.payload
            }
        default:
            throw new Error('Invalid actions.')
    }
}

export { initState };
export default reducer;